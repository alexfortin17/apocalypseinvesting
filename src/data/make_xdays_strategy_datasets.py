# -*- coding: utf-8 -*-
import click
import logging
from pathlib import Path
from dotenv import find_dotenv, load_dotenv
import multiprocessing as mp
import x_days_strategy
import holding_time_exploration

days_strat = [1,2,3,5,8,13,22]
#@click.command()
#@click.argument('input_filepath', type=click.Path(exists=True))
#@click.argument('output_filepath', type=click.Path())
def main(input_filepath, output_filepath):
    """ Runs data processing scripts to turn raw data from (../raw) into
        cleaned data ready to be analyzed (saved in ../processed).
    """
    logger = logging.getLogger(__name__)
    logger.info('making final data set from raw data')

    procs = []

    for d in days_strat:
        
        fds = mp.Process(target=x_days_strategy.main, args=(input_filepath, output_filepath, d,))
        procs.append(fds)
        fds.start()

    for proc in procs:
        proc.join()


if __name__ == '__main__':
    log_fmt = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    logging.basicConfig(level=logging.INFO, format=log_fmt)

    # not used in this stub but often useful for finding various files
    project_dir = Path(__file__).resolve().parents[2]

    # find .env automagically by walking up directories until it's found, then
    # load up the .env entries as environment variables
    load_dotenv(find_dotenv())
    inp = '~/Data/apocalypseinvesting/data/raw/Stocks'
    outp = '~/Data/apocalypseinvesting/data/interim'
    main(inp, outp)
