#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jan 10 20:10:01 2019

@author: alfo

Reads all Stocks CSV files and create one big Pandas Dataframe sorted by dates.
Create a pickle file out of the df.
"""

import os
import pandas as pd
import re
import dateutil.parser as dparser

pickle_result = "fullStocksDF.pkl"

def main(input_dirpath, output_dirpath):
    
    try:
        input_dirpath = os.path.expanduser(input_dirpath)
        output_dirpath = os.path.expanduser(output_dirpath)
        if not os.path.isdir(input_dirpath):
            raise Exception("{} is not a valid path".format(input_dirpath))
            
        if not os.path.isdir(output_dirpath):
            raise Exception("{} is not a valid path".format(output_dirpath))
        
        createGlobalDF(input_dirpath, output_dirpath)
            
    except Exception as ex:
        print(ex)
    

def createGlobalDF(input_dirpath, output_dirpath):
    globaldf = pd.DataFrame()
    stock_file_list = os.listdir(input_dirpath)
    
    for stock_file in stock_file_list:
        stock_name = re.search('(\w+)\..*', stock_file).groups()[0]
        print("Treating {}".format(stock_name))
        
        try:
            # read csv as dataframe
            df = pd.read_csv(os.path.join(input_dirpath, stock_file))
            df['Stock'] = stock_name
            globaldf = globaldf.append(df)
            
        except Exception as ex:
            print(ex)
            continue
    
    for index, row in globaldf.iterrows():
        if dparser.parse(row['Date']).year < 1990:
            globaldf.drop(index, inplace=True)
            
    globaldf.sort_values("Date", inplace=True)
    #reset the index to be in line with the reordering using dates
    globaldf = globaldf.reset_index(drop=True)
    # write resulting dataframe as a pickle
    globaldf.to_pickle(os.path.join(output_dirpath,pickle_result))
    return globaldf

if __name__ == "__main__":
    inp = '~/Data/apocalypseinvesting/data/raw/Stocks'
    outp = '~/Data/apocalypseinvesting/data/interim'
    main(inp, outp)