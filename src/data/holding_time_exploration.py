#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Jan  6 14:57:36 2019

Investigating how long it is preferable too hold on to a stock bought once it
lost more than 10% in a single day

@author: alfo
"""
import os
import pandas as pd
import dateutil.parser as dparser
import re

file_err_list = []
pickle_result = "holdTimeComparison.pkl"
def main(input_dirpath, output_dirpath):
    try:
        
        input_dirpath = os.path.expanduser(input_dirpath)
        output_dirpath = os.path.expanduser(output_dirpath)
        if not os.path.isdir(input_dirpath):
            raise Exception("{} is not a valid path".format(input_dirpath))
            
        if not os.path.isdir(output_dirpath):
            raise Exception("{} is not a valid path".format(output_dirpath))
        
        
        Search(input_dirpath, output_dirpath)
        return
    
    except Exception as ex:
        print(ex)

def performance(openp, closep):
    delta = closep - openp
    deltapercentage = delta/openp
    return deltapercentage
    
def Search(input_dirpath, output_dirpath):  
    stock_file_list = os.listdir(input_dirpath)
    result_obj = {'dates' : [],
              'stocks' : [],
              'volume': [],
              'oneday_gain_list' : [],
              'twodays_gain_list' : [],
              'threedays_gain_list' : [],
              'fivedays_gain_list' : [],
              'eightdays_gain_list' : [],
              'thirteendays_gain_list' : [],
              'twentytwodays_gain_list' : [],}
    
    for stock_file in stock_file_list:
        stock_name = re.search('(\w+)\..*', stock_file).groups()[0]
        print("Treating {}".format(stock_name))
        
        try:
            # read csv as dataframe
            df = pd.read_csv(os.path.join(input_dirpath, stock_file))
            for x in range(0, len(df.index) - 30):
                perf = performance(df.loc[x]['Open'], df.loc[x]['Close'])
                # Take action only when stock as a daily drop of more than 10%
                if perf <= -0.1:
                    # get year of occurence
                    year = dparser.parse(df.loc[x]['Date']).year
                    volume = (df.loc[x]['Volume'])
                    
                    if year < 1990 or volume < 10000:
                        continue
                    
                    result_obj['stocks'].append(stock_name)
                    result_obj['dates'].append(df.loc[x]['Date'])
                    result_obj['volume'].append(df.loc[x]['Volume'])
                    # 1st strategy is to buy before bell and
                    # sell before bell on next trading day
                    result_obj['oneday_gain_list'].append \
                    (performance(df.loc[x]['Close'], df.loc[x+1]['Close']))
                    # 2nd strategy is to buy before bell and
                    # sell before bell trading 2 days later
                    result_obj['twodays_gain_list'].append \
                    (performance(df.loc[x]['Close'], df.loc[x+2]['Close']))
                    
                    result_obj['threedays_gain_list'].append \
                    (performance(df.loc[x]['Close'], df.loc[x+3]['Close']))
                    
                    result_obj['fivedays_gain_list'].append \
                    (performance(df.loc[x]['Close'], df.loc[x+5]['Close']))
                    
                    result_obj['eightdays_gain_list'].append \
                    (performance(df.loc[x]['Close'], df.loc[x+8]['Close']))
                    
                    result_obj['thirteendays_gain_list'].append \
                    (performance(df.loc[x]['Close'], df.loc[x+13]['Close']))
                    
                    result_obj['twentytwodays_gain_list'].append \
                    (performance(df.loc[x]['Close'], df.loc[x+22]['Close']))
                    
        except Exception as ex:
            print(ex)
            file_err_list.append(stock_file)
            continue
        
    if not (len(result_obj['dates'])
        == len(result_obj['stocks']) \
        == len(result_obj['volume']) \
        == len(result_obj['oneday_gain_list']) \
        == len(result_obj['twodays_gain_list']) \
        == len(result_obj['threedays_gain_list']) \
        == len(result_obj['fivedays_gain_list']) \
        == len(result_obj['eightdays_gain_list']) \
        == len(result_obj['thirteendays_gain_list']) \
        == len(result_obj['twentytwodays_gain_list'])):
        
        print("PROBLEM!!! some list have different size!!")

    print("### RESULTS ###")
    print(result_obj)
    finaldf = pd.DataFrame(result_obj)
    # sort by dates inplace
    finaldf.sort_values("dates", inplace=True)
    #reset the index to be in line with the reordering using dates
    finaldf = finaldf.reset_index(drop=True)
    # write resulting dataframe as a pickle
    finaldf.to_pickle(os.path.join(output_dirpath,pickle_result))



