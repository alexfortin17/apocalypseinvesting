#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan 23 21:32:06 2019

@author: alfo
"""

"""Looking for the loss that should be endured since probabilities of bounce
back are still favorable eg. Buying at x to what minus y should you be selling
aka when is the probability of gain outweighed by the probability of a loss

Here we do a regression ewith gain on y axis and losses on x axis
We're interested in the x intercept, since this is the losses that will on 
average return a non negative gain"""

# 1. get the x day low
# 2. create row with x day low and final gain
# 3. plot with gain on Y and low on x
# 4. do regression
# 5. get x-intercept
import pickle
import argparse
import sklearn
import pandas as pd
import os


def main(pickle_path):
    try:
        pickle_path = os.path.expanduser(pickle_path)
        if not os.path.isfile(pickle_path):
            raise Exception("{} is not a valid path".format(pickle_path))
            
        df = pickle.load(open(pickle_path, 'rb'))
        
        #removing all iinstances with no negative returns
        negdf = pd.DataFrame()
        for index, row in df.iterrows():
            if row['x_days_low'] < 0:
                negdf = negdf.append(row)
        
        negdf = negdf.reset_index(drop=True)
        
        #removing upside outliers
        for index, row in negdf.iterrows():
            if row['x_days_gain'] > 0.5:
                negdf.drop(index, inplace=True)
                
        negdf = negdf.reset_index(drop=True)
        
        lowArray = negdf[['x_days_low']].values
        gainArray = negdf[['x_days_gain']].values
        
        # Doing linear regression on plot with gain on y axis and loss on x axis
        lm = sklearn.linear_model.LinearRegression()
        lm.fit(lowArray, gainArray)
        y_intercept = lm.intercept_
        slope = lm.coef_
        x_intercept = (0 - y_intercept)/slope
        
        print("x intercept is {}".format(x_intercept))
            
    except Exception as ex:
        print(ex)
    
    
if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("pickle_path", help="Path to the pickle file")
    args = parser.parse_args()
    main(args.pickle_path)