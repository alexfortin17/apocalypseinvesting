#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Jan 14 19:43:36 2019
Creates a dataframe and a pickle file representing all potential trades and
their result using the x days strategy
@author: alfo
"""
import os
import pandas as pd
import dateutil.parser as dparser
import re


file_err_list = []

def main(input_dirpath, output_dirpath, days):
    try:
        input_dirpath = os.path.expanduser(input_dirpath)
        output_dirpath = os.path.expanduser(output_dirpath)
        if not os.path.isdir(input_dirpath):
            raise Exception("{} is not a valid path".format(input_dirpath))
            
        if not os.path.isdir(output_dirpath):
            raise Exception("{} is not a valid path".format(output_dirpath))
        
        five_days_strategy(input_dirpath, output_dirpath, days)
            
    except Exception as ex:
        print(ex)


def performance(openp, closep):
    delta = closep - openp
    deltapercentage = delta/openp
    return deltapercentage
    
def five_days_strategy(input_dirpath, output_dirpath, days):
    
    result_obj = {'date' : [],
              'stock' : [],
              'volume': [],
              'x_days_high' : [],
              'x_days_low' : [],
              'x_days_gain' : [] }
    
    stock_file_list = os.listdir(input_dirpath)
    
    for stock_file in stock_file_list:
        stock_name = re.search('(\w+)\..*', stock_file).groups()[0]
        print("Treating {}".format(stock_name))
        
        try:
            # read csv as dataframe
            df = pd.read_csv(os.path.join(input_dirpath, stock_file))
            for x in range(0, len(df.index) - (days+2)):
                perf = performance(df.loc[x]['Open'], df.loc[x]['Close'])
                # Take action only when stock as a daily drop of more than 10%
                if perf <= -0.1:
                    # get year of occurence
                    year = dparser.parse(df.loc[x]['Date']).year
                    volume = df.loc[x]['Volume']
                    if year < 1990 or volume < 10000:
                        continue
                    
                    result_obj['stock'].append(stock_name)
                    result_obj['date'].append(df.loc[x]['Date'])
                    result_obj['volume'].append(df.loc[x]['Volume'])
                    
                    abs_high = 0.00
                    abs_low = 1000000.00
                    
                    for y in range(1,days+1):
                        if df.loc[x+y]['High'] > abs_high:
                            abs_high = df.loc[x+y]['High']
                    
                    for y in range(1,days+1):
                        if df.loc[x+y]['Low'] < abs_low:
                            abs_low = df.loc[x+y]['Low']
                        
                    result_obj['x_days_high'].append \
                    (performance(df.loc[x]['Close'], abs_high))
                    
                    result_obj['x_days_low'].append \
                    (performance(df.loc[x]['Close'], abs_low))
                    
                    
                    # buy before bell and sell before bell 5 trading days later
                    result_obj['x_days_gain'].append \
                    (performance(df.loc[x]['Close'], df.loc[x+days]['Close']))
                    
        except Exception as ex:
            print(ex)
            file_err_list.append(stock_file)
            continue
    
    if not (len(result_obj['date']) \
        == len(result_obj['stock']) \
        == len(result_obj['volume']) \
        == len(result_obj['x_days_high']) \
        == len(result_obj['x_days_low']) \
        == len(result_obj['x_days_gain'])):
        
        print("PROBLEM!!! some list have different size!!")
    finaldf = pd.DataFrame(result_obj)
    # sort by dates inplace
    finaldf.sort_values("date", inplace=True)
    #reset the index to be in line with the reordering using dates
    finaldf = finaldf.reset_index(drop=True)
    # write resulting dataframe as a pickle
    pickle_result = "{}_days_strat_trades.pkl".format(str(days))
    
    print(output_dirpath)
    print(pickle_result)
    finaldf.to_pickle(os.path.join(output_dirpath, pickle_result))
    
if __name__ == "__main__":
    main("~/Data/apocalypseinvesting/data/raw/Stocks",
         "~/Data/apocalypseinvesting/data/interim")
    




