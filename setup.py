from setuptools import find_packages, setup

setup(
    name='src',
    packages=find_packages(),
    version='0.1.0',
    description='Measuring investment strategies that leverage investors fears',
    author='Alex Fortin',
    license='MIT',
)
